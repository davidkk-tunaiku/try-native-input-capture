import React, { useEffect, useState } from "react";
import imageCompression from "browser-image-compression";
import "./Home.css";

const Home = () => {
  const [imageFile, setImageFile] = useState({
    original: null,
    compressed: null,
  });
  const [imageURL, setImageURL] = useState({
    original: null,
    compressed: null,
  });

  useEffect(() => {
    if (!imageFile.original) return;
    doImageCompression();
  }, [imageFile.original]);

  const doImageCompression = async () => {
    const options = {
      maxSizeMB: 1,
      maxWidthOrHeight: 1920,
    };
    try {
      const compressedImageFile = await imageCompression(
        imageFile.original,
        options
      );
      console.log(compressedImageFile);
      const compressedImageURL = URL.createObjectURL(compressedImageFile);

      setImageFile({
        ...imageFile,
        compressed: compressedImageFile,
      });

      setImageURL({
        ...imageURL,
        compressed: compressedImageURL,
      });
    } catch (e) {
      console.log(e);
    }
  };

  const onInputChange = (e) => {
    const imageFile = e.target.files[0];

    setImageFile({
      ...imageFile,
      original: e.target.files[0],
    });

    setImageURL({
      ...imageURL,
      original: URL.createObjectURL(imageFile),
    });
  };

  return (
    <div className="Home">
      <input
        type="file"
        id="imageFile"
        capture="environment"
        accept="image/*"
        onChange={(e) => onInputChange(e)}
      />
      <div>
        {imageURL.original && (
          <>
            <p>
              <strong>Original:</strong>
            </p>
            <img src={imageURL.original} height={250} />
            <p>Size: {`${imageFile.original.size / 1024 / 1024} MB`} </p>
          </>
        )}
        {imageURL.compressed && (
          <>
            <p>
              <strong>Compressed:</strong>
            </p>
            <img src={imageURL.compressed} height={250} />
            <p>Size: {`${imageFile.compressed.size / 1024 / 1024} MB`}</p>
          </>
        )}
      </div>
    </div>
  );
};

export default Home;
